gcodetools
==========

Extensión para que inkscape exporte trayectorias de Gcode.

Instalación
-----------

### GNU/Linux

Correr `instalar.sh` o copiar todos los archivos `.inx` y `.py` en el directorio `/usr/share/inkscape/extensions/` y después reiniciar inkscape

### Windows

Es necesario descomprimir y copiar todos los archivos de la carpeta gcodetools_es en el directorio:
Archivos de programa\Inkscape\share\extensions\ y después reiniciar inkscape

--- 

Al reiniciar inkscape te aparecerá un nuevo item en Menú/Extensiones/Gcodetools


